import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResStorePagePageRoutingModule } from './res-store-page-routing.module';

import { ResStorePagePage } from './res-store-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResStorePagePageRoutingModule
  ],
  declarations: [ResStorePagePage]
})
export class ResStorePagePageModule {}

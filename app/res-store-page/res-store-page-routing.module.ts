import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResStorePagePage } from './res-store-page.page';

const routes: Routes = [
  {
    path: '',
    component: ResStorePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResStorePagePageRoutingModule {}

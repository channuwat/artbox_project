import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResStorePagePage } from './res-store-page.page';

describe('ResStorePagePage', () => {
  let component: ResStorePagePage;
  let fixture: ComponentFixture<ResStorePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResStorePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResStorePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

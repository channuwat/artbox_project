import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IntroPagePageRoutingModule } from './intro-page-routing.module';

import { IntroPagePage } from './intro-page.page';
import { ImageShellComponent } from '../shell/image-shell/image-shell.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IntroPagePageRoutingModule,

  ],
  declarations: [IntroPagePage, ImageShellComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntroPagePageModule { }
